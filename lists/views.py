from django.shortcuts import redirect, render
from lists.models import Item, List
from django.core.exceptions import ValidationError

def home_page(request):

    items = Item.objects.all()
    message = 'Yey, waktunya berlibur'
    return render(request, 'home.html',  {'message': message})
def view_list(request, list_id):
    list_ = List.objects.get(id=list_id)
    items = Item.objects.filter(list=list_)
    count = items.count()
    message = ''
    if count == 0 :
        message = "Yey, waktunya berlibur"
    if count >0 & count <5:
        message = "Sibuk tapi santai"
    if count >=5 :
        message = "Oh Tidak"
    return render(request, 'list.html', {'list': list_,'message': message})

def new_list(request):
    list_ = List.objects.create()
    item = Item(text=request.POST['item_text'], list=list_)
    try:
        item.full_clean()
        item.save()
    except ValidationError:
        list_.delete()
        error = "You can't have an empty list item"
        return render(request, 'home.html', {"error": error})
    return redirect(list_)

def view_list(request, list_id):
    list_ = List.objects.get(id=list_id)
    error = None

    if request.method == 'POST':
        try:
            item = Item(text=request.POST['item_text'], list=list_)
            item.full_clean()
            item.save()
            return redirect(list_)
        except ValidationError:
            error = "You can't have an empty list item"

    return render(request, 'list.html', {'list': list_, 'error': error})